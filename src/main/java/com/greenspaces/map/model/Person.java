package com.greenspaces.map.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;


@Entity
public class Person {
    public Person(){}

    public Person(Double latitude, Double longitude, Integer height, Integer age, Date date, String gender, String info) {
        super();
        this.latitude = latitude;
        this.longitude = longitude;
        this.height = height;
        this.age = age;
        this.date = date;
        this.gender = gender;
        this.info = info;
    }
    @Id
    @Getter
    @Setter
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private long id;
    @Setter @Getter private Double latitude;
    @Setter @Getter private Double longitude;
    @Setter @Getter private Integer height;
    @Setter @Getter private Integer age;
    @Setter @Getter @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date date;
    @Setter @Getter private String gender;
    @Setter @Getter private String info;

}
