package com.greenspaces.map;

import com.greenspaces.map.controller.PersonController;
import com.greenspaces.map.controller.RestController;
import com.greenspaces.map.model.Person;
import com.greenspaces.map.repository.PersonRepository;
import javassist.tools.web.BadHttpRequest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.ui.Model;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
public class ControllerTest {
    @Autowired
    PersonRepository mockRepo;

    @Mock
    Model mockModel;

    @Test
    public void RestControllerTest() throws Exception {
        RestController mockController = new RestController(mockRepo);
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Person p1 = new Person(51.4579527, -2.6078847, 180, 25, sdf.parse("01/01/2019"), "male", "Sleeping bag");
        Person p2 = new Person(51.4589527, -2.6068847, 160, 35, sdf.parse("02/02/2019"), "female", "Guitar");

        // Add a new person with ID 1
        mockController.create2(p1);

        // Test findAll
        Long id = (long) 1;
        Iterable<Person> people = mockController.findAll();
        assertThat(people, iterableWithSize(1));
        for(Person p : people){
            assertEquals((Integer) 25, p.getAge());
            assertNotEquals((Integer) 24, p.getAge());
            id = p.getId();
        }

        // Test find
        Person p = mockController.find(id);
        assertEquals("Sleeping bag", p.getInfo() );
        assertNotEquals("Guitar" , p.getInfo());
        assertEquals(51.4579527, p.getLatitude() ,0);
        assertEquals(-2.6078847, p.getLongitude(),0);
        assertEquals((Integer) 180, p.getHeight());
        assertEquals(sdf.parse("01/01/2019"), p.getDate());



        // Test update
        mockController.update(id, p2);
        assertEquals("female", mockController.find(id).getGender() );
        assertNotEquals("male", mockController.find(id).getGender());
        try {
            mockController.update((long)20, p2);
            fail();
        } catch (BadHttpRequest e){
        }

        // Test delete
        mockController.delete(id);
        people = mockController.findAll();
        assertThat(people, iterableWithSize(0));
    }

    @Test
    public void PersonControllerTest() throws Exception {
        RestController mockRestController     = new RestController(mockRepo);
        PersonController mockPersonController = new PersonController(mockRepo);
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Person p1 = new Person(51.4579527, -2.5978847, 180, 25, sdf.parse("01/01/2019"), "male", "Sleeping bag");
        Person p2 = new Person(51.4589527, -2.5968847, 160, 25, sdf.parse("02/02/2019"), "female", "Guitar");

        mockRestController.create2(p1);
        mockRestController.create2(p2);

        // Test get all people in range - default arguments given
        ArrayList<Person> people = mockPersonController.People("-2.601854842696156", "51.44789102339354", "-2.5740671573022667", "51.46100803434112");
        assertThat(people, iterableWithSize(2));
        for(Person p : people){
            assertEquals((Integer) 25, p.getAge());
            assertNotEquals((Integer) 24, p.getAge());
        }

        // Test display person causes no exceptions
        try{
            assertEquals("index", mockPersonController.displayPerson(mockModel, "-2.601854842696156","51.44789102339354","-2.5740671573022667","51.46100803434112"));
        }catch(Exception e){
            fail();
        }
    }

}
