package com.greenspaces.map.controller;

import com.greenspaces.map.model.Person;
import com.greenspaces.map.repository.PersonRepository;
import javassist.tools.web.BadHttpRequest;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.*;

@org.springframework.web.bind.annotation.RestController
@RequestMapping(path = "/persons")
public class RestController {
    @Autowired
    private PersonRepository repo;

    public RestController(PersonRepository mock){
     repo = mock;
    }

    @GetMapping
    public Iterable<Person> findAll() {
        return repo.findAll();
    }

    @GetMapping(path = "/{id}")
    public Person find(@PathVariable("id") Long id) {
        return repo.findById(id).get();
    }

//    @PostMapping(path = "/add")
//    public Person create(@RequestParam("id") String id, @RequestParam("age") String age, @RequestParam("date") String date, @RequestParam("height") String height, @RequestParam("latitude") String latitude, @RequestParam("longitude") String longitude) {
//        Person p = new Person();
//        p.setAge(age);
//        p.setDate(date);
//        p.setHeight(height);
//        p.setLatitude(latitude);
//        p.setLongitude(longitude);
//        return repo.save(p);
//    }

    @PostMapping(consumes = "application/json")
    public Person create2(@RequestBody Person person) {
        return repo.save(person);
    }

    @DeleteMapping(path = "/{id}")
    public void delete(@PathVariable("id") Long id) {
        repo.deleteById(id);
    }

    @PutMapping(path = "/{id}")
    public Person update(@PathVariable("id") Long id, @RequestBody Person person) throws BadHttpRequest {
        if (repo.existsById(id)) {
            person.setId(id);
            return repo.save(person);
        } else {
            throw new BadHttpRequest();
        }
    }
}
