package com.greenspaces.map.controller;

import java.util.ArrayList;
import com.greenspaces.map.model.Person;
import com.greenspaces.map.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class PersonController {
    @Autowired
    private PersonRepository personRepository;

    public PersonController(PersonRepository mock){
        personRepository = mock;
    }

    @GetMapping("/")
    public String displayPerson(Model model, @RequestParam(value="Lon1", required=false, defaultValue="-2.601854842696156") String givenLon1,
                                @RequestParam(value="Lat1", required=false, defaultValue="51.44789102339354") String givenLat1,
                                @RequestParam(value="Lon2", required=false, defaultValue="-2.5740671573022667") String givenLon2,
                                @RequestParam(value="Lat2", required=false, defaultValue="51.46100803434112") String givenLat2) {
        Double Lon1 = Double.parseDouble(givenLon1);
        Double Lat1 = Double.parseDouble(givenLat1);
        Double Lon2 = Double.parseDouble(givenLon2);
        Double Lat2 = Double.parseDouble(givenLat2);
        ArrayList<Person> people = new ArrayList<>();
        for (Person p : personRepository.findAll()) {
            if (p.getLongitude() >= Lon1 && p.getLongitude() <= Lon2 &&
                    p.getLatitude() >= Lat1 && p.getLatitude() <= Lat2) {
                people.add(p);
//                model.addAttribute("latitude", p.getLatitude());
//                model.addAttribute("longitude", p.getLongitude());
//                model.addAttribute("height", p.getHeight());
//                model.addAttribute("age", p.getAge());
//                model.addAttribute("date", p.getDate());
            }
        }
        model.addAttribute("people", people);
        return "index";
    }

    @GetMapping("/bounds")
    @ResponseBody
    public ArrayList<Person> People(@RequestParam(value="Lon1", required=false, defaultValue="-2.601854842696156") String givenLon1,
                        @RequestParam(value="Lat1", required=false, defaultValue="51.44789102339354") String givenLat1,
                        @RequestParam(value="Lon2", required=false, defaultValue="-2.5740671573022667") String givenLon2,
                        @RequestParam(value="Lat2", required=false, defaultValue="51.46100803434112") String givenLat2){
        Double Lon1 = Double.parseDouble(givenLon1);
        Double Lat1 = Double.parseDouble(givenLat1);
        Double Lon2 = Double.parseDouble(givenLon2);
        Double Lat2 = Double.parseDouble(givenLat2);
        ArrayList<Person> people = new ArrayList<>();
        for (Person p : personRepository.findAll()) {
            if (p.getLongitude() >= Lon1 && p.getLongitude() <= Lon2 &&
                    p.getLatitude() >= Lat1 && p.getLatitude() <= Lat2) {
                people.add(p);
            }
        }
        return people;
    }


//gotta finish this, receive bounds of the currently viewed map from the user and then send points to be drawn accordingly.
//    @PostMapping("/")
//    public String getPeople(Model model, ) {
//
//
//
//    }
}
